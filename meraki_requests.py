'''Das meraki-SDK wird importiert'''
import meraki

'''
Diese Klasse verwendet das meraki-SDK zum Versenden von requests.
'''
class Meraki:
    '''
    Der Konstruktor (__init__) ist eine Funktion der Klasse,
        welche das Erstellen eines Objekts aus dieser managed.
    Der Parameter self wird verwendet, um auf das aktuelle Objekt zuzugreifen.
        So können auch sogenannte Instanzvariablen erzeugt werden. (self.varName = [...])
    Der Parameter apiKey wird verwendet, um dem Objekt den API-Key zu übergeben.
    '''
    def __init__(self, apiKey):
        self.dashboard = meraki.DashboardAPI(apiKey, suppress_logging=True)
        self.organizations = self.dashboard.organizations.getOrganizations()

    '''
    In dieser Methode erfolgt ein API-Call zur abfrage aller Netzwerke der Organisation
    '''
    def getNetworks(self, organizationName):
        id = self.getOrganization(organizationName)['id']
        networks = self.dashboard.organizations.getOrganizationNetworks(id)
        return networks

    '''
    Die "getDevices"-Methode fragt alle Geräte ab, die mit der übergebenen Organisation verknüpft sind.
    '''
    def getDevices(self, organizationName):
        id = self.getOrganization(organizationName)['id']
        devices = self.dashboard.organizations.getOrganizationDevicesStatuses(id)
        return devices

    '''
    Die "getNetworkName"-Methode liefert den Namen eines Netzwerks nach übergabe der networkID
    '''
    def getNetworkName(self, networkID):
        return self.dashboard.networks.getNetwork(networkID)['name']

    '''
    In dieser Methode werden alle Organisationen durchgegangen.
    Es wird verglichen, ob der übergebene Name enthalten ist.
    Ist das der Fall, wird die Organisation zurückgeliefert.
    Falls nicht wird eine Exception geworfen.
    '''
    def getOrganization(self, organizationName):
        for organization in self.organizations:
            if organization['name'] == organizationName:
                return organization
        raise Exception('Organization not found!\nOrganizations are:\n{}'.format(self.getAllOrganizationNamesAndIds()))

    '''
    Diese Methode liefert alle Organisationsnamen und IDs zurück, welche mit dem API-Key verbunden sind.
    '''
    def getAllOrganizationNamesAndIds(self):
        organizations = ""
        for organization in self.organizations:
            organizations += "{} - {}\n".format(organization['name'], organization["id"])
        return organizations
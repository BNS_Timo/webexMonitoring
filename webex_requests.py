'''Importieren des webexteamssdks von Cisco'''
from webexteamssdk import WebexTeamsAPI

'''Erstellen der Webex-Klasse'''
class Webex():

    '''
    Der Konstruktor (__init__) ist eine Funktion der Klasse,
        welche das Erstellen eines Objekts aus dieser managed.
    Der Parameter self wird verwendet, um auf das aktuelle Objekt zuzugreifen.
        So können auch sogenannte Instanzvariablen erzeugt werden. (self.varName = [...])
    Der Parameter botToken wird verwendet, um dem Objekt den Webex Token zu übergeben.
    '''
    def __init__(self, botToken):
        self.api = WebexTeamsAPI(botToken)
        self.all_rooms = self.api.rooms.list()

    '''
    In dieser Methode wird ein roomName als Parameter übergeben.
    Es wird geprüft, ob der Raum bereits vorhanden ist.
    Ist er vorhanden, wird die "checkRoom"-Methode aufgerufen, 
        um den Raum als Objekt zurückzuliefern.
    Ansonsten wird der Raum durch einen API Aufruf erstellt und zurückgeliefert.
    '''
    def createRoom(self, roomName):
        if self.checkRoom(roomName) != None:
            return self.checkRoom(roomName)
        else:
            room = self.api.rooms.create(roomName)
            return room

    '''
    In der "deleteRoom"-Methode wird ein roomName übergeben und geprüft, ob dieser vorhanden ist.
    Ist er vorhanden, wird dieser gelöscht.
    Ist er nicht vorhanden, wird die Nachricht "Room not found" zurückgeliefert.
    '''
    def deleteRoom(self, roomName):
        room = self.checkRoom(roomName)
        if room != "":
            self.api.rooms.delete(room.id)
            return "Room deleted"
        else:
            return "Room not found"

    '''
    Diese Methode ermöglicht das einfache Hinzufügen neuer Personen durch deren Mail Adresse.
    Durch die "checkRoom"-Methode wird geprüft, ob der Raum vorhanden ist.
    Falls es den Raum gibt, werden zwei API-Calls aufgerufen, welche zum einen die Person zum Raum
        hinzufügen und anschließend eine Willkommensnachricht in den Raum senden.
    Ansonsten wird "Room not found!" zurückgegeben.
    '''
    def addPersonToRoom(self, roomName, personEmail):
        room = self.checkRoom(roomName)
        if roomName != "":
            self.api.memberships.create(room.id, personEmail=personEmail)
            self.api.messages.create(room.id, text="Welcome to the room, {}!".format(personEmail))
        else:
            print("Room not found!")

    '''
    Diese Methode dient lediglich Entwicklern. 
    Es lassen sich alle Räume auf der Kommandozeile ausgeben, auf welche der Bot Zugriff hat.
    '''
    def printAllRooms(self):
        for room in self.all_rooms:
            print(room)

    '''
    Dieser Methode wird ein Raumname sowie eine Nachricht übergeben.
    Anschließend wird der Raum angelegt und die übergebene Nachricht gesendet.
    '''
    def sendMessageToRoom(self, roomName, message):
        room = self.createRoom(roomName)
        self.api.messages.create(room.id, text=message)

    '''
    Die "checkRoom"-Methode vergleicht den übergebenen Raumnamen mit allen vorhandenen Raumname.
    Ist der Raumname in den vorhandenen Namen enthalten, wird der Raum zurückgegeben.
    '''
    def checkRoom(self, roomName):
        for room in self.all_rooms:
            if room.title == roomName:
                return room

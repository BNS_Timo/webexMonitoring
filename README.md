# WebexMonitoring

## WICHTIG!
Dieses Projekt dient dem Zweck der Schulung.<br>
Es soll gezeigt werden, wie einfach mit der den APIs von Meraki sowie Webex gearbeitet werden kann.<br>
Die Software ist nicht dazu geeignet produktiv verwendet zu werden.<br>

## Einstieg

Wie ihr das Repository herunterladen könnt wird auf dieser Seite erklärt:<br>
https://docs.gitlab.com/ee/user/project/repository/#clone-a-repository

### Vorbereitung

Zum verwenden des Codes müsst ihr in dem Heruntergeladenen Repository eine neue<br> 
Virtual Environment (venv), eine ".env" Datei sowie eine Datei "main.py" erstellen.<br>

#### Venv

Zum Erstellen einer neuen venv wechselt ihr in der Git Bash in das Verzeichnis des heruntergeladenen Repositories<br>
und gebt folgendes ein:
````
python -m venv venv
````
Verwenden könnt ihr diese durch anschließende Eingabe von:

````
source /venv/bin/activate
````

Anschließend sollten neue Zeilen in eurer Git Bash mit ````(venv)```` beginnen.<br>
In dieser venv müsst ihr jetzt nur noch das ````dotenv```` Modul installieren.<br>
````
pip install dotenv
````

#### .env File

Die .env File wird dafür verwendet, den Meraki API Key sowie den Webex Token auszulagern.<br>
In dieser .env File müsst ihr die folgenden beiden Zeilen einfügen.<br>

````
apiKey="Hier könnte euer Meraki API Key stehen"
webexBotToken="Hier könnte der Token eueres Webex Bots stehen."
````

#### main.py

Da wir environment-Variablen auslesen müssen, die in der .env File gespeichert sind, wird das os-Modul benötigt.<br>
Außerdem muss der Bot und das dotenv-Modul importiert werden:

````
import os
from bot import Bot
from dotenv import load_dotenv
````

In diesem Fall ist die folgende Abfrage optional.<br>
Allerdings ist es besser, wenn ihr euch direkt angewöhnt diese zu verwenden.<br>

````
if __name__ == "__main__":
````

Diese Abfrage ist Sinnvoll, solltet ihr Module entwickeln.<br>
Dabei wird überprüft, ob die main.py direkt aufgerufen wird.
Mehr dazu erfahrt ihr unter:<br>
https://www.geeksforgeeks.org/what-does-the-if-__name__-__main__-do/

Anschließend müsst ihr die .env File laden und die Variablen eurer Umgebung in Python Variablen speichern.<br>

````
load_dotenv()
webexBotToken = str(os.getenv("webexBotToken"))
apiKey = str(os.getenv("apiKey"))
````

Nun ist alles bereit, damit der Bot verwendet werden kann.

# Verwendung

Der erste Schritt bei der Verwendung des Bots ist das Erzeugen eines Objektes, auch Instanziierung genannt.<br>

## Instanziierung

(Für ````<OrganizationName>```` und ````<WebexRoomName>```` solltet ihr natürlich entsprechende Werte eintragen.)

````
bot = Bot(apiKey, webexBotToken, "<OrganizationName>", "<WebexRoomName>")
````

Hierbei wird die ````__init__````-Methode, der sogenannte Konstruktor aufgerufen.<br>
In diesem werden die Bedingungen zum Erstellen eines Objekts aus dieser Klasse festgelegt und benötigte Informationen übergeben.<br>

## Methoden

Euer Bot-Objekt stellt nun folgenden Methoden zur verfügung:

* addPersonsToRoom(persons)
* sendInitialMessage()
* checkForNotOnlineDevices()
* sortDevices(devices)
* writeNotOnlineDevicesInWebex()
* run() oder run(duration)

Aufrufen könnt ihr diese mit der Punkt-Notation. Abhängig ob die Methoden einen Parameter benötigen oder nicht mit:

````
bot.<methodenName>()
oder
bot.<methodenName>(parameter)
````

### addPersonsToRoom(persons)

Mithilfe dieser Methode könnt ihr eine oder mehrere Personen in einen Raum hinzufügen.<br>
Dies ist abhängig davon, ob ihr einen String (````"email@example.com"````) <br>
oder eine Liste (````["email1@example.com", "email2@example.com" , ...]````) übergebt.

### sendInitalMessages()

Diese Methode sendet eine formatierte Nachricht in euren Raum.<br>
Enthalten ist der OrganizationName, die OrganizationID, die Anzahl der Netzwerke der Organisation sowie die Anzahl der darin geclaimten Geräte.
Sollten sich Geräte in einem anderen Status als "Online" befinden, wird eine weitere Nachricht gesendet. <br>
Diese enthält das entsprechende Gerät, in welchem Netzwerk sich dieses befindet, sowie den aktuellen Status.<br>

### checkForNotOnlineDevices()

In der Realität wäre dies eine private Methode. Aus Gründen der Einfachheit wurde dies hier nicht gemacht.<br>
Solltet ihr euch für private Methoden interessieren könnt ihr hier einen Artikel dazu lesen: <br>
https://www.geeksforgeeks.org/private-methods-in-python/

Die Methode könnt ihr dennoch in eurer main.py aufrufen (bzw. durch ````print(checkForNotOnlineDevices)```` ) <br>
und ihr werdet sehen, welche rückgabe durch diese Methode erfolgt.

### sortDevices(devices)

Diese Methode wäre ebenfalls eine private Methode (oder weiterführend sogar eine anonyme Funktion). <br>
Bei Interesse zu anonymen Funktionen: https://www.geeksforgeeks.org/python-lambda/ <br>
Verwendet wird diese lediglich in der ````checkForNotOnlineDevices````-Methode.<br>
Mithilfe dieser wird angegeben nach welchem Kriterium das Dictionary sortiert werden soll. (In diesem Fall 'name') <br>

### writeNotOnlineDevicesInWebex()

Diese Methode wird in der ````sendInitialMessages()````-Methode verwendet, um die Geräte mit Status und Netzwerk in den Webexraum zu posten. <br>

### run() bzw. run(duration)

Die ````run()````-Methode enthält einen optionalen Parameter. Das bedeutet, es wird ein default Wert für einen Parameter festgelegt. <br>
Hierdurch muss bei dem Verwenden der Methode nicht zwingend ein Parameter übergeben werden.<br>
Das kann die flexibilität einer solchen Methode erhöhen. <br>
In diesem Fall entscheidet der Parameter darüber, ob die while-Schleife auf eine übergebene Anzahl an Durchläufen begrenzt wird, <br>
oder ob die bis zur manuellen beendigung weiterläuft. <br>
Wird run ohne Parameter aufgerufen, läuft das Programm also in einer Dauerschleife. <br>
Wird eine Zahl, beispielsweise 10 übergeben, wird die Schleife zehn mal durchlaufen und anschließend beendet.<br>
Die Dauerschleife kann durch ````<strg> + <c>```` beendet werden.<br>
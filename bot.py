'''Importieren der benötigten Module und Klassen'''
from meraki_requests import Meraki
from webex_requests import Webex
import time

'''Erstellen der Bot-Klasse'''
class Bot:

    '''
    Der Konstruktor (__init__) ist eine Funktion der Klasse,
        welche das Erstellen eines Objekts aus dieser managed.
    Der Parameter self wird verwendet, um auf das aktuelle Objekt zuzugreifen.
        So können auch sogenannte Instanzvariablen erzeugt werden. (self.varName = [...])
    Die Parameter merakiApiKey und webexBotToken werden verwendet,
        um den API Key und Bot Token an das Objekt zu übergeben.
    Mit den Parametern organizationName und webexRoom
        werden die entsprechenden Namen übergeben.
    '''
    def __init__(self, merakiApiKey, webexBotToken, organizationName, webexRoom):
        self.meraki = Meraki(merakiApiKey)
        self.webex = Webex(webexBotToken)
        self.webexRoom = webexRoom
        self.organizationName = organizationName
        self.organization = self.meraki.getOrganization(organizationName)
        self.organizationNetworks = self.meraki.getNetworks(organizationName)
        self.organizationDevices = self.meraki.getDevices(organizationName)
        self.networksMessage = ""
        self.devicesMessage = ""

    '''
    Wie der Name bereits sagt, fügt diese Methode Personen zu einem Raum hinzu.
    Der Raum ist hierbei der, welcher bei der Erzeugung des Objekts übergeben wird.
    Falls ein String übergeben wird, wird lediglich eine Person hinzugefügt.
    Ansonsten wird versucht durch eine Liste zu iterieren und die Personen daraus hinzuzufügen.
    '''
    def addPersonsToRoom(self, persons):
        if "str" in str(type(persons)):
            self.webex.addPersonToRoom(self.webexRoom, persons)
        else:
            for person in persons:
                try:
                    self.webex.addPersonToRoom(self.webexRoom, person)
                except:
                    pass

    '''
    In dieser Methode wird eine initiale Nachricht erstellt, die den Namen der Organisation,
         die ID der Organisation, sowie die Anzahl der enthaltenen Netzwerke und Geräte ausgibt.
    Sollten in dieser Organisation Geräte in einem anderen Status als "Online" sein,
        wird eine Nachricht in den Raum gesendet und die Methode "writeNotOnlineDevicesInWebex" aufgerufen. 
    '''
    def sendInitialMessages(self):
        self.webex.sendMessageToRoom(self.webexRoom,
                                     f"The organization is called {self.organization['name']} "
                                     f"with the ID {self.organization['id']}\n"
                                     f"{self.organization['name']} contains {len(self.organizationNetworks)} "
                                     f"networks with a total of {len(self.organizationDevices)} devices.")
        if self.checkForNotOnlineDevices():
            self.webex.sendMessageToRoom(self.webexRoom, "Currently the following devices are not online")
            self.writeNotOnlineDevicesInWebex()

    '''
    In dieser Methode wird geprüft, welche Geräte sich nicht im Status "Online" befinden.
    Diese werden in die Liste "notOnlineDevices" gespeichert und zurückgegeben.
    '''
    def checkForNotOnlineDevices(self):
        notOnlineDevices = []
        for device in self.organizationDevices:
            if device['status'] != "online":
                notOnlineDevices.append(device)
        notOnlineDevices.sort(key=self.sortDevices)
        return notOnlineDevices

    '''
    Dies ist eine Hilfsmethode für die "checkForNotOnlineDevices"-Methode zur festlegung,
        nach welchem Key sortiert wird.
    '''
    def sortDevices(self, devices):
        return devices['name']

    '''
    In dieser Methode wird durch die Geräte iteriert, welche nicht Online sind.
    Diese werden mit Namen, Status und networkID in einen String gespeichert
        und letzlich in den Chat gepostet.
    Sollte es sich um mehr als 50 Geräte handeln wird die Nachricht aufgeteilt,
        damit die Zeichenbegrenzung von Webex keinen Fehler wirft.
    '''
    def writeNotOnlineDevicesInWebex(self):
        notOnlineDevicesString = ""
        sent = False
        for number, device in enumerate(self.checkForNotOnlineDevices()):
            sent = False
            notOnlineDevicesString += f"Device: {device['name']}, " \
                                      f"Status: {device['status']}, " \
                                      f"Network: {self.meraki.getNetworkName(device['networkId'])}\n"
            if number % 50 == 0 and number != 0:
                self.webex.sendMessageToRoom(self.webexRoom, notOnlineDevicesString)
                notOnlineDevicesString = ""
                sent = True
        if not sent:
            self.webex.sendMessageToRoom(self.webexRoom, notOnlineDevicesString)

    '''
    Diese Methode lässt den Bot in einer Dauerschleife laufen, damit regelmäßig geprüft wird,
        ob Geräte ihren Status ändern.
    Hierfür wird der aktuelle Status aller Geräte mit dem Status der 
        Instanzvariable self.organizationDevices verglichen.
    Der Status wird in einem String festgehalten, welcher anschließend in den Chat gepostet wird.
    Letztlich wird die Instanzvariable self.organizationName mit den aktuellen Werten aktualisiert.
    Dies ermöglicht die Feststellung der Statusänderungen im nächsten durchlauf der Schleife.
    '''
    def run(self, duration=True):
        timer = 0
        permanentLoop = False

        if "bool" in str(type(duration)):
            permanentLoop = True

        while timer < duration or permanentLoop:
            organizationDevicesNew = self.meraki.getDevices(self.organizationName)

            for number, device in enumerate(organizationDevicesNew):
                if device['status'] != self.organizationDevices[number]['status']:
                    self.webex.sendMessageToRoom(self.webexRoom,
                                                f"The device {device['name']} from network "
                                                f"{self.meraki.getNetworkName(device['networkId'])} "
                                                f"changed it's status to {device['status']}")
            self.organizationDevices = organizationDevicesNew
            time.sleep(1)
            timer += 1
            if timer % 10 == 0:
                print(timer)